

# download_substack.py
Originally posted at [justpaste.it](https://justpaste.it/8saym), also on [Gab](https://gab.com/how_do_I_based/posts/108302565069998870).
A script to archive entire substacks, just in case someone decides to blow their servers up.


## Everything not saved will be lost.

### A hostile attack on several substacks is imminent.

The reason for assuming this is [this](https://justpaste.it/redirect/8saym/https://nitter.net/conspirator0/status/1523859761384591364) tweet.

You must understand that the first signal of censors to attack is to document a list of dissenters.

I have seen this happen in the past:

- At best, these substacks will be more difficult to find on google.

- Alternatively, substack itself could be unlisted from search results.

- If this does not work, the substacks in particular will be targeted for censorship.

- If the webpage will not implement censorship, substack itself may be targeted directly though dirty attacks from the tech cabal / mafia.

*Removing payment processing services (if using something like paypal), removing them from wherever they are hosted at (if using AWS), or attacking them directly with a DDOS attack... All of these and more has already happened to Gab.*

### The substacks mentioned / targeted:

https://stevekirsch.substack.com
https://viralimmunologist.substack.com
https://rwmalonemd.substack.com
https://palexander.substack.com
https://technofog.substack.com
https://alexberenson.substack.com
https://steady.substack.com
https://igorchudov.substack.com
https://grahamlinehan.substack.com
https://boriquagato.substack.com
https://caitlinjohnstone.substack.com
https://robertreich.substack.com
https://bariweiss.substack.com
https://jessicar.substack.com
https://taibbi.substack.com
https://danielvdtuin.substack.com
https://emeralddb3.substack.com
https://draculadaily.substack.com
https://jamesroguski.substack.com
https://ragingoptimist.substack.com
https://gregolear.substack.com
https://greenwald.substack.com
https://sarahburwick.substack.com
https://noahpinion.substack.com

**Everything not saved will be lost.**
So, you can use this script to save your favorite substacks.
*In fact, you should.*


## Usage

Install required software:

`$ sudo apt install selenium chromium pip`

Install python libraries:

`$ python3 -m pip install selenium BeautifulSoup4 xhtml2pdf lxml;`

Download all substacks:

`$ python3 download_substack.py -o=~/Downloads https://stevekirsch.substack.com https://viralimmunologist.substack.com https://rwmalonemd.substack.com https://palexander.substack.com https://technofog.substack.com https://alexberenson.substack.com https://steady.substack.com https://igorchudov.substack.com https://grahamlinehan.substack.com https://boriquagato.substack.com https://caitlinjohnstone.substack.com https://robertreich.substack.com https://bariweiss.substack.com https://jessicar.substack.com https://taibbi.substack.com https://danielvdtuin.substack.com https://emeralddb3.substack.com https://draculadaily.substack.com https://jamesroguski.substack.com https://ragingoptimist.substack.com https://gregolear.substack.com https://greenwald.substack.com https://sarahburwick.substack.com https://noahpinion.substack.com`

Download only one substack:

`$ python3 download_substack.py https://noahpinion.substack.com`

You can also download to .html, .md, and not download pdfs at all through these commands:

`$ python3 download_substack.py https://noahpinion.substack.com -md -html -noPdf`
