from pathlib import Path
import sys

# scraping
from selenium import webdriver
from selenium.webdriver.common.by import By

# downloading webpages
from bs4 import BeautifulSoup
import requests
from time import sleep

# multithreading
from threading import Thread
import queue

usage = """
Usage:
python3 download_substack.py someone.substack.com someoneElse.substack.com

You can also download an article alone instead of an entire substack.

Optional arguments:
    -o=<path>
        Set the download path to something other than the downloads folder

    -html
        Write out the html for the downloaded articles

    -md
        Write out a markdown file aside from a .pdf

    -noPdf
        Do not write a pdf file (you only want html or markdown files)

    -firefox
        Use FireFox for downloading instead of Chrome

Requirements
 - You have to install selenium and Chrome or Chromium for this to work:
https://selenium-python.readthedocs.io/installation.html

 - run: python3 -m pip install selenium BeautifulSoup4 xhtml2pdf lxml
If you want to download to markdown format, add markdownify as well
"""

q = queue.Queue()

output = Path.home()/'Downloads'
doMd = False
doHtml = False
doPdf = True
firefox = False
workDone = False


def main(args):
    parseArguments(args)

    # exit conditions
    if not output.exists():
        print('Err: Folder %s does not exist.' % output)
        sys.exit(1)

    if not doPdf and not doMd and not doHtml:
        print('Specify at least one format for the output.')
        sys.exit(2)

    # Start parallel downloader thread
    t = Thread(target=articleDownloadThread)
    t.start()

    # Meanwhile, scrape all articles on the main thread.
    scrapeArticles(args)
    q.join()


def parseArguments(args):
    global output, doMd, doHtml, doPdf, firefox

    delete = []
    for i, arg in enumerate(args):
        if arg.startswith('-o='):
            path = arg.replace('-o=', '')
            if '~' in path:
                output = Path.home() / path.replace('~/', '')
            else:
                output = Path()
            delete.append(arg)
        if arg.startswith('-html'):
            doHtml = True
            delete.append(arg)
        if arg.startswith('-md'):
            doMd = True
            delete.append(arg)
        if arg.startswith('-noPdf'):
            doPdf = False
            delete.append(arg)
        if arg.startswith('-firefox'):
            firefox = False
            delete.append(arg)
    for deli in delete:
        args.remove(deli)


def scrapeArticles(args):
    global workDone
    with webdriver.Chrome() if not firefox else webdriver.Firefox() as driver:
        for url in args:

            if '/p/' in url:
                q.put_nowait((url, output, url[url.find('/p/') + 3:]))
                continue

            directory = output / url.removeprefix('https://')
            driver.get('%s/archive' % url)
            print('Scraping: %s' % url)

            last_len = 0
            links = []
            while True:
                driver.execute_script('window.scrollBy(0, 99999);')
                sleep(0.1)  # sometimes breaks without this

                for a in driver.find_elements(By.TAG_NAME, 'a'):
                    href = a.get_property('href')
                    if '/p/' in href \
                    and '/comments' not in href \
                    and '/coming-soon' not in href \
                    and href not in links:
                        filename = href[href.find('/p/') + 3:]
                        q.put_nowait((href, directory, filename))
                        links.append(href)

                if last_len == len(links): break
                else: last_len = len(links)
            print('Found %d articles at %s.' % (len(links), url))
    print('Found all articles from links provided.')
    workDone = True


def articleDownloadThread():
    while True:
        if q.unfinished_tasks != 0:
            (a, directory, filename) = q.get()
            webpageToPdf(a, directory, filename)
            q.task_done()
        else:
            if workDone:
                print('Work is done.')
                break
            else:
                print('Waiting for a bit.')
                sleep(2)


def webpageToPdf(url, directory, filename):
    try:
        filepath = directory/filename
        filepathStr = str(filepath)
        pdfExists = Path(filepathStr + '.pdf').exists()
        htmlFile = Path(filepathStr + '.html')
        htmlExists = htmlFile.exists()
        mdFile = Path(filepathStr + '.md')
        mdExists = mdFile.exists()

        # Stop if the files exist
        if doPdf == pdfExists and doMd == mdExists and doHtml == htmlExists:
            print('Skipping\t%s\tas it already exists at\t%s.' % (url, filename))
            return
        print('Downloading\t%s\tto\t%s' % (url, filepathStr))

        # Try to get resource until it bonks us.
        res = requests.get(url)
        while res.status_code == 429:
            print('Too many requests, waiting...')
            sleep(31) # Typically enough
            res = requests.get(url)

        if res.status_code != 200:
            print('Error when scraping %s code:%d contents:\n%s'
                  % (url, res.status_code, res.text))
            return

        soup = BeautifulSoup(res.content, 'lxml')
        article = soup.article

        if article is None:
            # TODO: deal with comments-page type articles
            # article = soup.find(class_='comments-page')
            print('Reading of comments page is not yet implemented: %s' % url)
            return

        if article is None:
            print('Error when trying to scrape, cannot read: %s' % url)
            return

        decompose(article.find(class_='modal-table'))

        footer = article.find(class_='post-footer')
        author_header = article.find(class_='meta-author-wrap')

        origin = soup.new_tag('a', href=url)
        origin.string = url
        insert_after(author_header, origin)

        if footer is None: # likely paid
            subscribe = article.find(class_='paywall')
            insert_before(author_header, soup.new_tag("hr"))
            insert_after(subscribe, author_header)
        else:
            insert_after(footer, author_header)
            replace_with(footer, soup.new_tag("hr"))

        if not directory.exists(): directory.mkdir()
        if doHtml and not pdfExists:
            htmlFile.write_text(article.prettify())

        if doMd and not mdExists:
            from markdownify import markdownify
            mdFile.write_text(markdownify(str(article)))

        if doPdf and not pdfExists:
            from xhtml2pdf import pisa
            with open(filepathStr + '.pdf', 'w+b') as out:
                # TODO: guest articles don't work
                try:
                    pisa.CreatePDF(str(article), dest=out)
                except Exception as ex:
                    print('Cannot convert webpage to pdf, is it a guest article? %s:' % url)
                    print(ex)

        print('Done!')
    except Exception as ex:
        print('Could not download article from %s:' % url)
        print(ex)


def decompose(node):
    if node is None: return
    node.decompose()


def clear(node):
    if node is None: return
    node.clear()


def insert_after(receiverNode, insertedNode):
    if receiverNode is None or insertedNode is None: return
    receiverNode.insert_after(insertedNode)


def insert_before(receiverNode, insertedNode):
    if receiverNode is None or insertedNode is None: return
    receiverNode.insert_before(insertedNode)


def replace_with(node, string):
    if node is None: return
    node.replace_with(string)


if __name__ == '__main__':
    if 'help' in sys.argv or 'h' in sys.argv or len(sys.argv) == 1:
        print(usage)
        sys.exit(0)
    main(sys.argv[1:])
